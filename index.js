// console.log("Hello World");


fetch("https://jsonplaceholder.typicode.com/todos", {method: "GET"})
	.then(response => {
		console.log(response);
		return response.json()}
		)

	.then(data => {
		let title = data.map(element => element.title)
		console.log(title);
	})


fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "GET"})
		.then(response => response.json())
		.then(result => console.log(result));


console.log('The item "delectus aut autem" on the list has a status of false');


fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			userId: 1,
			id: 1,
			title: "Created To Do List Item",
			completed: false,
		})
	})
		.then(response => response.json())
		.then(result => console.log(result));


fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			dateCompleted: "Pending",
			description: "To update the my to do list with a different data structure.",
			id: 1,
			status: "Pending",
			title: "Updated To Do List Item",
			userId: 1,
		})
	})
		.then(response => response.json())
		.then(result => console.log(result));


fetch("https://jsonplaceholder.typicode.com/todos/1", {
		method: "PATCH",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			completed: false,
			dateCompleted: "07/09/21",
			id: 1,
			status: "complete",
			title: "delectus aut autem",
			userId: 1,
		})
	})	
		.then(response => response.json())
		.then(result => console.log(result));


fetch("https://jsonplaceholder.typicode.com/todos/1", {
				method: "DELETE"})
		.then(response => response.json())
		.then(result => console.log(result));